import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './organization/layout/main-layout/main-layout.component';
import { OrganizationBasicInfoComponent } from './organization/components/organization-basic-info/organization-basic-info.component';

const routes: Routes = [
  {
    path : "organization",
    loadChildren : () => import("./organization/organization.module").then(
      (m) => m.OrganizationModule
    )
  },
  {
    path : "workforce-admin",
    loadChildren : () => import("./workforce-admin/workforce-admin.module").then(
      (m) => m.WorkforceAdminModule
    )
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
