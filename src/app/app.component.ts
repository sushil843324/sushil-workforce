import { Component, OnInit } from '@angular/core';
import { SideNavControlService } from "./organization/layout/services/side-nav.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'edaiva-workforce';

  constructor(
    private readonly sideNavControl: SideNavControlService,
  ) {}

  ngOnInit(): void {

    this.sideNavControl.setSideNavItems([
      {
        type: 'link',
        label: 'Home',
          path: '/organization',
          iconPath: 'assets/icons/dashboard.png',
          visible:true
        },
      {
          type: 'dropdown',
          label: 'Jobs',
          iconPath: 'assets/icons/dashboard.png',
          visible:true,
          children : [
            {
              type : "link",
              label : "All jobs",
              path : "/organization/jobs/list-jobs"
            },
            {
              type : "link",
              label : "Add job",
              path : "/organization/jobs/post-job-form"
            }
          ]
      },
      {
          type: 'dropdown',
          label: 'Proposals',
          iconPath: 'assets/icons/dashboard.png',
          visible:true,
          children : [
            {
              type : "link",
              label : "List Proposals",
              path : "/organization/proposals/list-proposals"
            }
          ]
      },
  ]);
    
  }

}
