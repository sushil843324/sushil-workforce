import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AccessRefreshInterceptor } from './organization/authentication/services/access-refresh.interceptor';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
],
  providers: [
    {
      provide : HTTP_INTERCEPTORS,
      multi : true,
      useClass : AccessRefreshInterceptor
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
