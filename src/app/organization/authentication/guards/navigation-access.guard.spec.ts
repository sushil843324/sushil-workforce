import { TestBed } from '@angular/core/testing';

import { NavigationAccessGuard } from './navigation-access.guard';

describe('NavigationAccessGuard', () => {
  let guard: NavigationAccessGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(NavigationAccessGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
