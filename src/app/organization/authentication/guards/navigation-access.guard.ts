import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { OrganizationService } from '../../services/organization.service';

@Injectable({
  providedIn: 'root'
})
export class NavigationAccessGuard implements CanActivate {

  constructor(
    private router : Router,
    private organizationService : OrganizationService
  ) {}


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
      // if a member of organizaiton or create-organization form is submitted then show home, 
      // otherwise, redirect to /create-organization

      if(this.organizationService.isOrganizationCreated.getValue())
        return true;
      else{
        this.router.navigate(["/organization/create-organization"])
        return false;
      }
  }
  
}
