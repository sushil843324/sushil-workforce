import { TestBed } from '@angular/core/testing';

import { AccessRefreshInterceptor } from './access-refresh.interceptor';

describe('AccessRefreshInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      AccessRefreshInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: AccessRefreshInterceptor = TestBed.inject(AccessRefreshInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
