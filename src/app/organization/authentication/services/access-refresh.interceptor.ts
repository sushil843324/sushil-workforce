import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, catchError, finalize, switchMap, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Injectable()
export class AccessRefreshInterceptor implements HttpInterceptor {

  constructor(
    private auth : AuthService,
    private cookie : CookieService,
    private router  : Router
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      finalize(() => {
        // Nothing to do here
      }), 
      catchError((err) => {
        if(err.status == 401 && err.error.code == "token_not_valid")
          return this.reRequestWithRefresh(request, next);
        if(err.status == 401 && err.error.code == "refresh_token_not_valid") {
          this.auth.isRefreshing = false;
          this.auth.clearUserSession();
          this.router.navigate(["/login"]);
          return throwError(err);
        }

        return throwError(err);
      })
    )
  }

  reRequestWithRefresh(request: HttpRequest<any>, next: HttpHandler) {
    let user = JSON.parse(this.cookie.get('user'));
    if (!this.auth.isRefreshing) {
        this.auth.isRefreshing = true;

        return this.auth.callRefreshRequest().pipe(
            switchMap((tokenRes: any) => {
                this.auth.updateRefreshAccess(
                    tokenRes.refresh,
                    tokenRes.access
                );
                this.auth.isRefreshing = false;
                return next.handle(
                    this.addTokenHeader(request, tokenRes.access)
                );
            }),
            catchError((err: any) => {
                console.log(err);
                if (
                    err.status == 401 &&
                    err.error.code === 'refresh_token_not_valid'
                ) {
                    this.auth.isRefreshing = false;
                    this.auth.logout();
                    return throwError(err);
                }
                return throwError(err);
            })
        );
    }
    return this.intercept(this.addTokenHeader(request, user.access), next);
}

  private addTokenHeader(request: HttpRequest<any>, token: string) {
    return request.clone({
        headers: request.headers.set('Authorization', `Bearer ${token}`),
    });
}
}
