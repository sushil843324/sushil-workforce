import { HttpClient, HttpHeaderResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { MemberCheck, UserData } from '../../interfaces/organization-types';
import { environment } from 'src/environments/environment';
import { map, shareReplay } from 'rxjs';
import { UserSession } from '../../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http : HttpClient,
    private cookie : CookieService
  ) { }

  isRefreshing = false;

  readonly memberCheck$ = this.http.get<MemberCheck>(`${environment.workforceUrl}/member-check`).pipe(shareReplay(1));

  readonly userData$ = this.http
      .get<UserData>(`${environment.workforceUrl}/companies/member`)
      .pipe(shareReplay(1));

  readonly isAdmin$ = this.userData$.pipe(map((user) => user.admin));
  readonly companyId$ = this.memberCheck$.pipe(map((data) => data.company));

  changePassword(oldPassword : string, newPassword : string) {
    let user : any = this.cookie.get("user");
    user = JSON.parse(user);
    let token = user["access"];

    const header = new HttpHeaders({
      Authorization : `Bearer ${token}`
    })

    return this.http.patch(
      `${environment.authUrl}/user/change-password`,
      {
        old_password : oldPassword,
        new_password : newPassword
      },
      {headers : header}
    )
    .pipe(
      map((res) => {
        console.log(res);
        return res;
      })
    )
  }

  logout() {
    let user : any;
    if (this.cookie.check("user")) {
      user = JSON.parse(this.cookie.get("user"));
    }

    return this.http
      .post(`${environment.authUrl}/user/logout`,{
        refresh : user?.refresh
      })
      .pipe(
        map(
          (res) => {
            this.cookie.deleteAll("/", environment.cookie_domain);
            return res;
          }
        )
      )
  }

  getLoggedInuser() {
    const cookie = JSON.parse(this.cookie.get("user"));
    return cookie?.user;
  }

  callRefreshRequest() {
    let user : any;
    if (this.cookie.check("user")) {
      user = JSON.parse(this.cookie.get("user"));
    }

    return this.http.post(`${environment.authUrl}/user/token/refresh`,{
      refresh : user.refresh
    });
  }

  updateRefreshAccess(refresh : any, access : any) {
    let user : any;
    if(this.cookie.check("user"))
      user = JSON.parse(this.cookie.get("user"));

    let newVersion = user;
    newVersion.refresh = refresh;
    newVersion.access = access;
    this.cookie.set(
      "user",
      JSON.stringify(newVersion),
      null!,
      "/",
      environment.cookie_domain
    )
    
    return;

  }

  get AuthHeader() {
    let user : any;
    if (this.cookie.check("user")) {
      user = JSON.parse(this.cookie.get("user"));
    }
    return new HttpHeaders ({
      Authorization : 'Bearer ' + user?.access
    });
  }

  clearUserSession() {
    this.cookie.set(
        'user',
        JSON.stringify(new UserSession()),
        null!,
        '/',
        environment.cookie_domain,
    );
}

}
