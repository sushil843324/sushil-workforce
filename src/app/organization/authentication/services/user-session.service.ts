import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root',
})
export class UserSessionService {
    private readonly userSession = new BehaviorSubject<any | null>(null);

    constructor(private readonly cookie: CookieService) {
        this.setUserData(this.getUserSession());
    }

    getUserSession() {
        if (this.cookie.check('user')) {
            return JSON.parse(this.cookie.get('user'));
        }
        return null;
    }

    setUserData(userData: any) {
        localStorage.setItem('userData', JSON.stringify(userData));
        this.userSession.next(userData);
    }

    getUserData() {
        return this.userSession.asObservable();
    }
}
