import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrganizationService } from '../../services/organization.service';

@Component({
  selector: 'app-create-organization',
  templateUrl: './create-organization.component.html',
  styleUrls: ['./create-organization.component.scss']
})
export class CreateOrganizationComponent implements OnInit {

  constructor(
    private router : Router,
    private service : OrganizationService
  ) { }

  ngOnInit(): void {
  }

  moveToHome() {
    this.service.isOrganizationCreated.next(true);
    this.router.navigate(["/organization"])
    console.log("wokring");
    
  }

}
