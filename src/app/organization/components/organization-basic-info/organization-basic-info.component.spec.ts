import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationBasicInfoComponent } from './organization-basic-info.component';

describe('OrganizationBasicInfoComponent', () => {
  let component: OrganizationBasicInfoComponent;
  let fixture: ComponentFixture<OrganizationBasicInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizationBasicInfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrganizationBasicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
