export interface ApiResponse {
    statusCode: number,
    data: any; 
    message: string,
    success: boolean
}

export interface MemberCheck {
    role : "learner" | "organization" | "admin",
    company : string
}

export interface UserData {
    address1: string;
    address2?: string;
    admin: boolean;
    city: string;
    company: string;
    company_name: string;
    company_website: string;
    country: string;
    created_date: string;
    dob: string;
    email: string;
    firstname: string;
    id: string;
    is_affiliate: boolean;
    lastname: string;
    mobile?: string;
    organization_type: string;
    phone_number: {
        Home?: number;
        Mobile?: number;
        Office?: number;
    };
    pincode: string;
    profilepic?: string;
    state: string;
    user: string;
}
