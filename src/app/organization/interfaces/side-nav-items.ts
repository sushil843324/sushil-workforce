export type SideNavItem = SideNavLinkItem | SideNavDropdownItem;

export interface SideNavLinkItem extends BaseItem {
    type: 'link';
    path: string;
}

export interface SideNavDropdownItem extends BaseItem {
    type: 'dropdown';
    children: SideNavLinkItem[];
}

interface BaseItem {
    type: string;
    label: string;
    iconPath?: string;
    visible?:boolean;
}
