export class UserSession {
    user: User;
    access: string;
    refresh: string;
    session_active: boolean = false;
    constructor(data?: { user: any; access: string; refresh: string }) {
        this.user = new User(data?.user);
        this.access = data?.access!;
        this.refresh = data?.refresh!;
        if (data?.refresh) {
            this.session_active = true;
        }
    }
}

class User {
    active: boolean;
    admin: boolean;
    created_date: Date;
    email: string;
    firstname: string;
    id: string;
    is_company_admin: boolean;
    is_recruiter: boolean;
    last_login: Date;
    last_name: string;
    normal_user: boolean;
    recruiter_id: string;
    staff: boolean;

    constructor(data: {
        active: boolean;
        admin: boolean;
        created_date: Date;
        email: string;
        firstname: string;
        id: string;
        is_company_admin: boolean;
        is_recruiter: boolean;
        last_logi: Date;
        last_name: string;
        normal_user: boolean;
        recruiter_id: string;
        staff: boolean;
    }) {
        this.active = data?.active;
        this.admin = data?.admin;
        this.created_date = data?.created_date;
        this.email = data?.email;
        this.firstname = data?.firstname;
        this.id = data?.id;
        this.is_company_admin = data?.is_company_admin;
        this.is_recruiter = data?.is_recruiter;
        this.last_login = data?.last_logi;
        this.last_name = data?.last_name;
        this.normal_user = data?.normal_user;
        this.recruiter_id = data?.recruiter_id;
        this.staff = data?.staff;
    }
}