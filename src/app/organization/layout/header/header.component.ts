import { Component,  EventEmitter,  Output } from '@angular/core';
import { SideNavControlService } from '../services/side-nav.service';
// import { AuthService } from 'src/app/authentication/services/auth.service';
// import { UserSessionService } from 'src/app/authentication/services/user-session.service';
// import { UserData } from 'src/app/interfaces/user-member';
import { Router } from '@angular/router';
// import { ToastsService } from 'src/app/components/toasts/toasts.service';
import { environment } from 'src/environments/environment';
import { LogoutConfirmationDialogComponent } from '../logout-confirmation-dialog/logout-confirmation-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
    @Output() loggedOut = new EventEmitter();
    user!:any; 

    constructor(
        public readonly sideNavControl: SideNavControlService,
        private router:Router,
        private modalService: NgbModal

        // private readonly toasts: ToastsService,
        // private auth:AuthService,
        //private userData: UserSessionService,

    ) {
            // this.userData.getUserData().subscribe({
            //     next: value => console.log(value)
            // })
        }


    logout() {
        const confirmationModal = this.modalService.open(
            LogoutConfirmationDialogComponent,
          {
            keyboard: false,
            backdrop: 'static',
            modalDialogClass: 'rounded-4 overflow-hidden',
            centered: true
          }
        );
        confirmationModal.componentInstance.message = 'Are you sure you want to Logout?';

        confirmationModal.result.then((confirmation: boolean) => {
          if (confirmation) {
            alert("code for actual api is commented bro, Fix it");
          //   this.auth.logout().subscribe({
          //     next: res => {
          //         window.location.href =
          //         environment.auth_portal_url +
          //         '?navigateTo=' +
          //         environment.learning_path_portal_url;
          //         this.toasts.show("Logged out sucessfully","success")
          //     },
          //     error: err => {
          //         console.error(err);
          //         window.location.href =
          //         environment.auth_portal_url +
          //         '?navigateTo=' +
          //         environment.learning_path_portal_url;
          //     }
          // })
          }
        });
      }
}
