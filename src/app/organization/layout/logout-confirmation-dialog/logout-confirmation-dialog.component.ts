import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout-confirmation-dialog',
  templateUrl: './logout-confirmation-dialog.component.html',
  styleUrls: ['./logout-confirmation-dialog.component.scss']
})
export class LogoutConfirmationDialogComponent{

  constructor(
    private modal: NgbActiveModal
    ) { }

  ngOnInit(): void {
  }

  submit(confirmation: boolean) {
    this.modal.close(confirmation);
  }

  add(confirmation: boolean) {
    this.modal.close(confirmation)
  }
}
