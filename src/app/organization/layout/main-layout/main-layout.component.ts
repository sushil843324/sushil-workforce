import { Component, OnInit } from '@angular/core';
import { SideNavControlService } from '../services/side-nav.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor(
    public readonly sideNavControl : SideNavControlService
  ) { }

  ngOnInit(): void {
  }

}
