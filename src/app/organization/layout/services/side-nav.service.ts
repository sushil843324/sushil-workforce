import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SideNavItem } from '../../interfaces/side-nav-items';

@Injectable({
    providedIn: 'root',
})
export class SideNavControlService {
    public readonly _isSideNavExpanded$ = new BehaviorSubject(true);
    private readonly _navItems$ = new BehaviorSubject<SideNavItem[]>([]);

    readonly isSideNavExpanded$ = this._isSideNavExpanded$.asObservable();
    readonly navItems$ = this._navItems$.asObservable();

    Toggle : string = "Collapse";

    toggleSideNav() {
        this._isSideNavExpanded$.next(!this._isSideNavExpanded$.value);

        if(this._isSideNavExpanded$.getValue())
            this.Toggle = "Collapse";
        else
            this.Toggle = "Expand";
    }

    setSideNavIsExpanded(isExpanded: boolean) {
        this._isSideNavExpanded$.next(isExpanded);
    }

    setSideNavItems(navItems: SideNavItem[]) {
        this._navItems$.next([...navItems]);
    }
}
