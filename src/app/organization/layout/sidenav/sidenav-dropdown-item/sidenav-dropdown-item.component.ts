import { Component, Input, OnInit } from '@angular/core';
import { SideNavDropdownItem } from '../../../interfaces/side-nav-items';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-sidenav-dropdown-item',
    templateUrl: './sidenav-dropdown-item.component.html',
    styleUrls: ['./sidenav-dropdown-item.component.scss'],
})
export class SidenavDropdownItemComponent implements OnInit {
    @Input() dropdownItem!: SideNavDropdownItem;

    collapsed = true;

    constructor(private readonly route: ActivatedRoute) {}

    ngOnInit(): void {
        if (
            this.dropdownItem.children.find(
                (item : any) =>
                    item.path.slice(1) ===
                    this.route.snapshot.firstChild?.url?.[0]?.path
            )
        ) {
            this.collapsed = false;
        }
    }
}
