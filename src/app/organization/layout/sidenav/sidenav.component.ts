import { Component } from '@angular/core';
import { SideNavControlService } from '../services/side-nav.service';

@Component({
    selector: 'app-sidenav',
    templateUrl: './sidenav.component.html',
    styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent {
    constructor(
        public readonly sideNavControl: SideNavControlService
    ) {}
}
