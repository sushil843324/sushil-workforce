import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { OrganizationBasicInfoComponent } from './components/organization-basic-info/organization-basic-info.component';
import { CreateOrganizationComponent } from './components/create-organization/create-organization.component';
import { NavigationAccessGuard } from './authentication/guards/navigation-access.guard';

const routes: Routes = [
  {
    path : "",
    component : MainLayoutComponent,
    children : [
      {
        path : "",
        component : OrganizationBasicInfoComponent,
        canActivate : [NavigationAccessGuard]
      },
      {
        path : "create-organization",
        component : CreateOrganizationComponent
      },
      {
        path : "jobs",
        loadChildren : () => import("./pages/jobs/jobs.module").then(
          (m) => m.JobsModule
        )
      },
      {
        path : "proposals",
        loadChildren : () => import("./pages/proposals/proposals.module").then(
          (m) => m.ProposalsModule
        )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationRoutingModule { }
