import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationRoutingModule } from './organization-routing.module';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { LogoutConfirmationDialogComponent } from './layout/logout-confirmation-dialog/logout-confirmation-dialog.component';
import { CreateOrganizationComponent } from './components/create-organization/create-organization.component';
import { OrganizationBasicInfoComponent } from './components/organization-basic-info/organization-basic-info.component';
import { SidenavComponent } from './layout/sidenav/sidenav.component';
import { SidenavDropdownItemComponent } from './layout/sidenav/sidenav-dropdown-item/sidenav-dropdown-item.component';
import { ReactiveFormsModule } from '@angular/forms';




@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    MainLayoutComponent,
    LogoutConfirmationDialogComponent,
    CreateOrganizationComponent,
    OrganizationBasicInfoComponent,
    SidenavComponent,
    SidenavDropdownItemComponent
  ],
  imports: [
    CommonModule,
    OrganizationRoutingModule,
    ReactiveFormsModule
  ]
})
export class OrganizationModule { }
