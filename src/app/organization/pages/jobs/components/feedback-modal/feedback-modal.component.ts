import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { JobsService } from '../../services/jobs.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-feedback-modal',
  templateUrl: './feedback-modal.component.html',
  styleUrls: ['./feedback-modal.component.scss']
})
export class FeedbackModalComponent implements OnInit {

  constructor(
    private fb : FormBuilder,
    private service : JobsService,
    private modal : NgbActiveModal
  ) { }

  @Input() projectId : any

  form! : FormGroup
  userRating: number = 0;
  isSubmitted = false
  companyId = `65b8717c8efed806d9ed807e`

  ngOnInit(): void {
    this.form = this.fb.nonNullable.group({
      posted_by : ["", [Validators.required]],
      designation : ["", [Validators.required]],
      feedback : ["", [Validators.required]],
      ratings : [0, [Validators.required]]
    })
  }

  setUserRating(rating: number) {
    if(this.userRating === rating)
      this.userRating = rating - 1;
    else
      this.userRating = rating;
    
  }

  // Function to determine if a star should be filled
  isStarFilled(starNumber: number): boolean {    
    return starNumber <= this.userRating;
  }

  onSubmit() {
    this.form.patchValue({
      ratings : this.userRating
    })

    console.log(this.form.value, "form data");
    

    this.isSubmitted = true;
    this.service.postFeedback(this.form.value, this.companyId, this.projectId).subscribe((res) => {
      console.log("feedback posted!");
      this.modal.close(true);
    })
  }

}
