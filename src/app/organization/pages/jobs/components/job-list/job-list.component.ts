import { Component, OnInit } from '@angular/core';
import { JobsService } from '../../services/jobs.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FeedbackModalComponent } from '../feedback-modal/feedback-modal.component';

@Component({
  selector: 'app-list-jobs',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.scss']
})
export class JobListComponent implements OnInit {

  constructor(
    private service : JobsService,
    private modal : NgbModal
  ) { }

  jobs : any = []
  companyId = `65b8717c8efed806d9ed807e`
  sort = "latest"

  ngOnInit(): void {
    this.service.temporaryFormDetails = [];
    this.getJobList();
  }

  getJobList() {
    this.service.getJobList(this.companyId, this.sort).subscribe((res) => {
      this.jobs = res?.data;
    })
  }

  async openFeedbackModal(id:any) {
    const modal = this.modal.open(FeedbackModalComponent, {
      size : "md"
    });

    modal.componentInstance.projectId = id;

    const res = await modal.result;
    if(res)
      this.getJobList();

    
  }

  sortList(event:any) {
    this.sort = event.target.value;
    this.getJobList();
  }

}
