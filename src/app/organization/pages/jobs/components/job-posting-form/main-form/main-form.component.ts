import { Component, OnInit } from '@angular/core';
import { JobsService } from '../../../services/jobs.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-job-form',
  templateUrl: './main-form.component.html',
  styleUrls: ['./main-form.component.scss']
})
export class MainFormComponent implements OnInit {

  constructor(
    private router : Router,
    private service : JobsService
  ) { }

  step = 1
  createJobFormDetails : Record<string, any> = {}

  // Fetch companyId from the cookies
  companyId = `65b8717c8efed806d9ed807e`

  ngOnInit(): void {
  }

  onNext(data : any) {

    this.createJobFormDetails = {
      ...this.createJobFormDetails,
      ...data
    }

    if(this.step == 3) {
      
      Object.keys(this.createJobFormDetails).forEach((key) => {
        if(key == "job_skills")
          data.append(key, JSON.stringify(this.createJobFormDetails[key]));
        else
          data.append(key, this.createJobFormDetails[key])
      })
      
      data.append('company_id' , this.companyId)

      this.service.createJob(data).subscribe((res) => {
        this.router.navigate(["organization/jobs/list-jobs"])
      })
    }else{
      this.step++;
    }
  }

  onBack() { 
      this.step = this.step - 1;
  }

}
