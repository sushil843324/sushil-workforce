import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { JobsService } from '../../../services/jobs.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-job-form-step-1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss']
})
export class jobPostStep1 implements OnInit {

  constructor(
    private router : Router,
    private fb : FormBuilder,
    private service : JobsService
  ) { }

  @Output() submitted = new EventEmitter<any>() 


  categories = [
    "Category1", "Category2", "Category3", "Category4"
  ]

  subCategories = [
    "subCategory1", "subCategory2", "subCategory3", "subCategory4"
  ]
  
  form! : FormGroup

  
  ngOnInit(): void {
    this.form = this.fb.nonNullable.group({
      job_title : ["", [Validators.required]],
      job_deadline : ["", [Validators.required]],
      job_location : ["", [Validators.required]],
      job_category : ["", [Validators.required]],
      job_subCategory : ["", [Validators.required]],
    })

    const data = this.service.temporaryFormDetails[0] || {};
    this.form.patchValue({
      job_title : data?.job_title,
      job_deadline : data?.job_deadline,
      job_location : data?.job_location,
      job_category : data?.job_category,
      job_subCategory : data?.job_subCategory,
    })
  }


  onNext() {
    if(this.service.temporaryFormDetails[0]){
      this.service.temporaryFormDetails.splice(0, 1);
    }
    this.service.temporaryFormDetails.push(this.form.value);

    this.submitted.next(this.form.value);
    
  }

  // onSubmit() {
  //     // console.log(this.form.value, "working here");
      
  //     this.service.createJobFormDetails = {
  //       ...this.form.value
  //     }

  //     // console.log(this.service.createJobFormDetails, "yes working bro");
      

  //     this.router.navigate(["organization/add-job-details"])
  // }



}
