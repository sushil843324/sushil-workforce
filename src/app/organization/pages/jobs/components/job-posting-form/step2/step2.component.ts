import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JobsService } from '../../../services/jobs.service';

@Component({
  selector: 'app-job-form-step-2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class jobPostStep2 implements OnInit {

  constructor(
    private router : Router,
    private fb : FormBuilder,
    private service : JobsService
  ) { }

  @ViewChild('selectElement') selectElement!: ElementRef;

  @Output() submitted = new EventEmitter<any>()
  @Output() changeStep = new EventEmitter<any>()

  skills = [
    "HTML", "CSS", "JavaScript", "NodeJs", "TypeScript", "AWS", "NextJs", "Angular", "Vue", "QA Testing", "Network Engineering"
  ]

  selectedSkills : any[] = []

  form! : FormGroup
  
  ngOnInit(): void {
    console.log(this.service.temporaryFormDetails, 'here is data');

    this.form = this.fb.nonNullable.group({
      job_resources_required : ["", [Validators.required]],
      job_duration : ["", [Validators.required]],
      job_experience : ["", [Validators.required]],
      job_stipend : ["", [Validators.required]],
      job_skills : [[""]]
    })

    const data = this.service.temporaryFormDetails[1] || {};
    this.form.patchValue({
      job_resources_required : data?.job_resources_required,
      job_duration : data?.job_duration,
      job_experience : data?.job_experience,
      job_stipend : data?.job_stipend,
      job_skills : data?.job_skills,
    })
  }

  selectSkill(event:any) {
    if(!this.selectedSkills.includes(event.target.value))
        this.selectedSkills.push(event.target.value);
    this.selectElement.nativeElement.selectedIndex = 0;
  }

  onNext() {
    this.form.patchValue({
      job_skills : this.selectedSkills
    })

    if(this.service.temporaryFormDetails[1]){
      this.service.temporaryFormDetails.splice(1, 1);
    }
    this.service.temporaryFormDetails.push(this.form.value);

    this.submitted.next(this.form.value)

    
  }

  moveBack(){
    this.changeStep.next(0);
  }

}
