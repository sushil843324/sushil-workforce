import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { JobsService } from '../../../services/jobs.service';

@Component({
  selector: 'app-job-form-step-3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss']
})

export class jobPostStep3 implements OnInit {

  constructor(
    private fb : FormBuilder,
    private router : Router,
    private service : JobsService
  ) { }

  @Output() submitted = new EventEmitter<any>()
  @Output() changeStep = new EventEmitter<any>()

  fd = new FormData()
  form! : FormGroup
  jobDescriptionFiles : any
  isSubmitted = false


  ngOnInit(): void { 
    this.form = this.fb.nonNullable.group({
      job_description : ["", [Validators.required]]
    })   
  }


  selectFiles(event : any) {
    const files = event.target.files;
    
    this.jobDescriptionFiles = Array.from(files);


    for(let file of files)
      this.fd.append("files", file);
  }

deleteImage(index: number) {
    this.jobDescriptionFiles.splice(index, 1); 
   
}


  onSubmit() {
    this.isSubmitted = true;
    Object.keys(this.form.value).forEach((key) => {
      this.fd.append(key, this.form.value[key]);
    })    
    this.submitted.next(this.fd);

  }

  onBack() {
      this.changeStep.next(0);
  }

}
