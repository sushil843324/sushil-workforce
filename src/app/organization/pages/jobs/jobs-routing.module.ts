import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobListComponent } from './components/job-list/job-list.component';
import { jobPostStep1 } from './components/job-posting-form/step1/step1.component';
import { jobPostStep2 } from './components/job-posting-form/step2/step2.component';
import { jobPostStep3 } from './components/job-posting-form/step3/step3.component';
import { MainFormComponent } from './components/job-posting-form/main-form/main-form.component';

const routes: Routes = [
  {
    path : "list-jobs",
    component : JobListComponent
  },
  {
    path : "post-job-form",
    component : MainFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobsRoutingModule { }
