import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobsRoutingModule } from './jobs-routing.module';
import { jobPostStep1 } from './components/job-posting-form/step1/step1.component';
import { jobPostStep2 } from './components/job-posting-form/step2/step2.component';
import { jobPostStep3 } from './components/job-posting-form/step3/step3.component';
import { JobListComponent } from './components/job-list/job-list.component';
import { MainFormComponent } from './components/job-posting-form/main-form/main-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FeedbackModalComponent } from './components/feedback-modal/feedback-modal.component';


@NgModule({
  declarations: [
    jobPostStep1,
    jobPostStep2,
    jobPostStep3,
    JobListComponent,
    MainFormComponent,
    FeedbackModalComponent
  ],
  imports: [
    CommonModule,
    JobsRoutingModule,
    ReactiveFormsModule
  ]
})
export class JobsModule { }
