import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from '../../../interfaces/organization-types';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JobsService {

  temporaryFormDetails : any[] = []

  constructor(
    private http : HttpClient
  ) { }


  getPublishedJobs(companyId : any) {
    return this.http.get(`${environment}/recruiter/jobs/list/${companyId}`)
  }

  
  getJobList(id : any, order : any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/recruiter/jobs/requests/list?organizationId=${id}&sort=${order}`
      )
    }

  createJob(payload : any) {
    return this.http.post(
      `${environment.workforceUrl}/recruiter/jobs/requests/add`,
      payload
  )}

  postFeedback(payload:any, companyId:any, projectId:any) {
    return this.http.post(
      `${environment.workforceUrl}/recruiter/feedbacks/add?companyId=${companyId}&jobId=${projectId}`,
      payload
    )}
  }

