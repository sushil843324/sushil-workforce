import { Component, Input, OnInit } from '@angular/core';
import { ProposalsService } from '../../services/proposals.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-accept-modal',
  templateUrl: './accept-modal.component.html',
  styleUrls: ['./accept-modal.component.scss']
})
export class AcceptModalComponent implements OnInit {

  constructor(
    private service : ProposalsService,
    private activeModal : NgbActiveModal
  ) { }

  @Input() proposalId : any

  ngOnInit(): void {
  }

  acceptProposal() {
    this.service.acceptProposal(this.proposalId).subscribe((res) => {
      console.log("proposal accepted!");
      this.activeModal.close(true);
    })
  }

  closeModal() {
    this.activeModal.close(false);
  }

}
