import { Component, OnInit } from '@angular/core';
import { ProposalsService } from '../../services/proposals.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AcceptModalComponent } from '../accept-modal/accept-modal.component';
import { RejectModalComponent } from '../reject-modal/reject-modal.component';

@Component({
  selector: 'app-list-proposals',
  templateUrl: './list-proposals.component.html',
  styleUrls: ['./list-proposals.component.scss']
})
export class ListProposalsComponent implements OnInit {

  constructor(
    private service : ProposalsService,
    private modal : NgbModal
  ) { }

  sort : string = "latest"
  proposals : any

  ngOnInit(): void {
    this.listProposals();
  }

  listProposals() {
      this.service.getProposalsList(this.sort).subscribe((res) => {
        this.proposals = res.data;
      })
  }

  sortList(event : any) {
    this.sort = event.target.value;
    this.listProposals();
  }

  async openAcceptModal(id:any) {
    const modal = this.modal.open(AcceptModalComponent, {
      size : "sm"
    })

    modal.componentInstance.proposalId = id;

    const res = await modal.result;
    if(res)
      this.listProposals();
  }

  async openRejectModal(id:any) {
    const modal = this.modal.open(RejectModalComponent, {
      size : "sm"
    })

    modal.componentInstance.proposalId = id;

    const res = await modal.result;
    if(res)
      this.listProposals();
    
  }

}
