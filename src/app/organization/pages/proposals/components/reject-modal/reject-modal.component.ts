import { Component, Input, OnInit } from '@angular/core';
import { ProposalsService } from '../../services/proposals.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-reject-modal',
  templateUrl: './reject-modal.component.html',
  styleUrls: ['./reject-modal.component.scss']
})
export class RejectModalComponent implements OnInit {

  constructor(
    private service : ProposalsService,
    private fb : FormBuilder,
    private activeModal : NgbActiveModal
  ) { }

  @Input() proposalId : any

  form! : FormGroup

  ngOnInit(): void {
    this.form = this.fb.nonNullable.group({
      reason : ["", [Validators.required]],
      description : ["", [Validators.required]]
    })
  }

  closeModal() {
    this.activeModal.close(false);
  }

  onSubmit() {
    this.service.rejectProposal(this.form.value, this.proposalId).subscribe((res) => {
      console.log("proposal rejected!");
      this.activeModal.close(true);
    })
    
  }

}
