import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListProposalsComponent } from './components/list-proposals/list-proposals.component';

const routes: Routes = [
  {
    path : "list-proposals",
    component : ListProposalsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProposalsRoutingModule { }
