import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProposalsRoutingModule } from './proposals-routing.module';
import { ListProposalsComponent } from './components/list-proposals/list-proposals.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AcceptModalComponent } from './components/accept-modal/accept-modal.component';
import { RejectModalComponent } from './components/reject-modal/reject-modal.component';


@NgModule({
  declarations: [
    ListProposalsComponent,
    AcceptModalComponent,
    RejectModalComponent
  ],
  imports: [
    CommonModule,
    ProposalsRoutingModule,
    ReactiveFormsModule
  ]
})
export class ProposalsModule { }
