import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from 'src/app/organization/interfaces/organization-types';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProposalsService {

  constructor(
    private http : HttpClient
  ) { }

  // GET

  getProposalsList(order : any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/recruiter/proposals/list?sort=${order}`
    )
  }

  acceptProposal(id:any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/recruiter/proposals/accept?proposalId=${id}`
    )
  }

  rejectProposal(payload : any, id : any) {
    return this.http.post<ApiResponse>(
      `${environment.workforceUrl}/recruiter/proposals/reject?proposalId=${id}`,
      payload
    )
  }





}
