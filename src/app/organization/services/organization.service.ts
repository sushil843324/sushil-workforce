import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  constructor() { }

  isOrganizationCreated = new BehaviorSubject<boolean>(false);
}
