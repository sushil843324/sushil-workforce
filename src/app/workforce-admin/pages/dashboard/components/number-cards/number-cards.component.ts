import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-number-cards',
  templateUrl: './number-cards.component.html',
  styleUrls: ['./number-cards.component.scss']
})
export class NumberCardsComponent implements OnInit {

  constructor() { }

  @Input() requests : any
  @Input() publishedProjects : any
  @Input() acceptedProposals : any
  @Input() sentProposals : any

  ngOnInit(): void {
  }

}
