import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-recent-applications',
  templateUrl: './recent-applications.component.html',
  styleUrls: ['./recent-applications.component.scss']
})
export class RecentApplicationsComponent implements OnInit {

  constructor() { }

  @Input() applicationsList : any;

  ngOnInit(): void {
  }

}
