import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentProposalsComponent } from './recent-proposals.component';

describe('RecentProposalsComponent', () => {
  let component: RecentProposalsComponent;
  let fixture: ComponentFixture<RecentProposalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecentProposalsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RecentProposalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
