import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-recent-proposals',
  templateUrl: './recent-proposals.component.html',
  styleUrls: ['./recent-proposals.component.scss']
})
export class RecentProposalsComponent implements OnInit {

  constructor() { }

  @Input() proposalsList : any

  ngOnInit(): void {
  }

}
