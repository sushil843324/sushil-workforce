import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-recent-requests',
  templateUrl: './recent-requests.component.html',
  styleUrls: ['./recent-requests.component.scss']
})
export class RecentRequestsComponent implements OnInit {

  constructor() { }

  @Input() requestsList : any;

  ngOnInit(): void {
  }

}
