import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { NumberCardsComponent } from './components/number-cards/number-cards.component';
import { RecentProposalsComponent } from './components/recent-proposals/recent-proposals.component';
import { RecentRequestsComponent } from './components/recent-requests/recent-requests.component';
import { RecentApplicationsComponent } from './components/recent-applications/recent-applications.component';
import { MainComponent } from './main/main.component';


@NgModule({
  declarations: [
    NumberCardsComponent,
    RecentProposalsComponent,
    RecentRequestsComponent,
    RecentApplicationsComponent,
    MainComponent  
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
