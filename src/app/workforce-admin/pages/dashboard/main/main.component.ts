import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../services/dashboard.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(
    private service : DashboardService
  ) { }

  proposals : any;
  applications : any;
  requests : any;

  totalRequests : any;
  totalPublishedProjects : any;
  totalProposalsSent : any;
  totalProposalsAccepted : any;

  ngOnInit(): void {
    this.listProposals();
    this.listApplications();
    this.listRequests();
    this.listPublishedProjectsList();
  }



  listProposals() {
    this.service.getProposalList().subscribe((res) => {
      this.proposals = res.data;

      this.totalProposalsAccepted = this.proposals.filter((item : any) => item.status == "accepted").length;
      this.totalProposalsSent = this.proposals.filter((item : any) => item.status == "pending").length;
    })
  }

  listApplications() {
    this.service.getApplicationsList().subscribe((res) => {
      this.applications = res.data;
    })
  }

  listRequests() {
    this.service.getRequestsList().subscribe((res) => {
      this.requests = res.data;

      this.totalRequests = this.requests.length;
    })
  }

  listPublishedProjectsList() {
    this.service.getPublishedProjectsList().subscribe((res) => {
      this.totalPublishedProjects = res.data.length;
    })
  }

}
