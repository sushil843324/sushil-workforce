import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from 'src/app/workforce-admin/interfaces/apiResponse';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private http : HttpClient
  ) { }

  getProposalList() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/proposals/list`
    )
  }

  getApplicationsList() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/jobs/applications/list`
    )
  }

  getRequestsList() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/jobs/requests/list`
    )
  }

  getPublishedProjectsList() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/jobs/published/list`
    )
  }

}
