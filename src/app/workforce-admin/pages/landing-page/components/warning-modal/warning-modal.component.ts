import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-warning-modal',
  templateUrl: './warning-modal.component.html',
  styleUrls: ['./warning-modal.component.scss']
})
export class WarningModalComponent implements OnInit {

  constructor(
    private activeModal : NgbActiveModal
  ) { }

  ngOnInit(): void {
  }

  discard() {
    this.activeModal.close(false);
  }

  next() {
    this.activeModal.close(true);
  }


}
