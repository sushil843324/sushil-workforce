import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LandingPageService } from '../../services/landing-page.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { WarningModalComponent } from '../../components/warning-modal/warning-modal.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-banner-section',
  templateUrl: './banner-section.component.html',
  styleUrls: ['./banner-section.component.scss']
})
export class BannerSectionComponent implements OnInit {

  constructor(
    private fb : FormBuilder,
    private service : LandingPageService,
    private modal : NgbModal,
    private router : Router
  ) { }

  form! : FormGroup
  fd = new FormData()
  bannerImages : any[] = []
  bannerSectionData : any
  isSaved = false
  initialFormValue : any;
  
  ngOnInit(): void {

    this.getBannerSectionData();

    this.form = this.fb.nonNullable.group({
      heading : ["", [Validators.required]],
      tagline : ["", [Validators.required]],
      buttonName : ["", [Validators.required]],
      buttonLink : ["", [Validators.required]],
      leftBoxData : this.fb.nonNullable.group({
        numbers : [0, [Validators.required]],
        description : ["", [Validators.required]]
      }),
      rightBoxData : this.fb.nonNullable.group({
        numbers : [0, [Validators.required]],
        description : ["", [Validators.required]]
      })
    })

  }

  isFormUnchanged(): boolean {
    return JSON.stringify(this.form.value) === JSON.stringify(this.initialFormValue);
  }
  
  selectedFiles(event:any) {
    const files = event.target.files;
    this.bannerImages = Array.from(files);
    
    for(let file of files)
      this.fd.append("files", file);
  }

  onSubmit() {
      Object.keys(this.form.value).forEach((key) => {
        if(key === "leftBoxData" || key === "rightBoxData")
          this.fd.append(key, JSON.stringify(this.form.value[key]))
        else
          this.fd.append(key, this.form.value[key])
      })

      this.service.addBannerSectionData(this.fd).subscribe((res) => {
        console.log("banner section data posted!");
        this.isSaved = true;
      })

      this.form.reset;
  }

  async onNext() {
    if(!this.isFormUnchanged() && !this.isSaved){
      console.log("yes changed");
      
      const modal = this.modal.open(WarningModalComponent, {
        size : "lg"
      })

      const res = await modal.result;

      if(res){
        this.router.navigate(["/workforce-admin/landing-page/sliding-logos"]);
      }
    }else{
        this.router.navigate(["/workforce-admin/landing-page/sliding-logos"]);
    }
  }

  getBannerSectionData() {
    this.service.getBannerSectionData().subscribe((res) => {
      this.bannerSectionData = res.data;

      this.form.patchValue({
        heading : this.bannerSectionData?.heading,
        tagline : this.bannerSectionData?.tagline,
        buttonName : this.bannerSectionData?.buttonName,
        buttonLink : this.bannerSectionData?.buttonLink,
        leftBoxData : this.bannerSectionData?.leftBoxData,
        rightBoxData : this.bannerSectionData?.rightBoxData
      })

      this.bannerImages = this.bannerSectionData?.bannerImages;
      this.initialFormValue = this.form.value;
    })

  }

  deleteImage(index : any) {
    this.bannerImages.splice(index, 1);
  }

}
