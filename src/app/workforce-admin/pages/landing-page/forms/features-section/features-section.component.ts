import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LandingPageService } from '../../services/landing-page.service';
import { WarningModalComponent } from '../../components/warning-modal/warning-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-features-section',
  templateUrl: './features-section.component.html',
  styleUrls: ['./features-section.component.scss']
})
export class FeaturesSectionComponent implements OnInit {

  constructor(
    private fb : FormBuilder,
    private service : LandingPageService,
    private modal : NgbModal,
    private router : Router
  ) { }

  form! : FormGroup
  fd = new FormData()
  featureImage : any
  featuresSectionData : any
  isSaved = false
  initialFormValue : any

  ngOnInit(): void {
    this.getFeatureSectionData();

    this.form = this.fb.nonNullable.group({
      smallHeading : ["", [Validators.required]],
      mainHeading : ["", [Validators.required]],
      description : ["", [Validators.required]],
      buttonName : ["", [Validators.required]],
      buttonLink : ["", [Validators.required]],
      feature1 : ["", [Validators.required]],
      feature2 : ["", [Validators.required]],
      feature3 : ["", [Validators.required]],
      feature4 : ["", [Validators.required]]
    })
  }

  getFeatureSectionData() {
      this.service.getFeaturesSectionData().subscribe((res) => {
        this.featuresSectionData = res.data;

        this.form.patchValue({
          smallHeading : this.featuresSectionData?.smallHeading,
          mainHeading : this.featuresSectionData?.mainHeading,
          description : this.featuresSectionData?.description,
          buttonName : this.featuresSectionData?.buttonName,
          buttonLink : this.featuresSectionData?.buttonLink,
          feature1 : this.featuresSectionData?.feature1,
          feature2 : this.featuresSectionData?.feature2,
          feature3 : this.featuresSectionData?.feature3,
          feature4 : this.featuresSectionData?.feature4,
        })
        this.featureImage = this.featuresSectionData?.featureImage
        this.initialFormValue = this.form.value;
      })

  }

  onSubmit() {
    Object.keys(this.form.value).forEach((key) => {
      this.fd.append(key, this.form.value[key])
    })

    this.service.addFeaturesSectionData(this.fd).subscribe((res) => {
      console.log("features section data posted!");
      this.isSaved = true;
    })
  }

  selectedFiles(event : any) {
    const file = event.target.files[0];
    this.featureImage = file;
    this.fd.append("files", file)

  }

  isFormUnchanged(): boolean {
    return JSON.stringify(this.form.value) === JSON.stringify(this.initialFormValue);
  }

  async next() {
    if(!this.isFormUnchanged() && !this.isSaved){
      const modal = this.modal.open(WarningModalComponent, {
        size : "lg"
      })

      const res = await modal.result;

      if(res){
        this.router.navigate(["/workforce-admin/landing-page/steps"]);
      }
    }else{
        this.router.navigate(["/workforce-admin/landing-page/steps"]);
    }
  }

  async previous() {
    if(!this.isFormUnchanged() && !this.isSaved){
      const modal = this.modal.open(WarningModalComponent, {
        size : "lg"
      })

      const res = await modal.result;

      if(res){
        this.router.navigate(["/workforce-admin/landing-page/sliding-logos"]);
      }
    }else{
        this.router.navigate(["/workforce-admin/landing-page/sliding-logos"]);
    }
  }

  deleteImage() {
    this.featureImage = null;
  }

}
