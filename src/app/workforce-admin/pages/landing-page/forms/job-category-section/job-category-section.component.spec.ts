import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobCategorySectionComponent } from './job-category-section.component';

describe('JobCategorySectionComponent', () => {
  let component: JobCategorySectionComponent;
  let fixture: ComponentFixture<JobCategorySectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobCategorySectionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JobCategorySectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
