import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { LandingPageService } from '../../services/landing-page.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { WarningModalComponent } from '../../components/warning-modal/warning-modal.component';

@Component({
  selector: 'app-job-category-section',
  templateUrl: './job-category-section.component.html',
  styleUrls: ['./job-category-section.component.scss']
})
export class JobCategorySectionComponent implements OnInit {

  constructor(
    private service : LandingPageService,
    private fb : FormBuilder,
    private modal : NgbModal,
    private router : Router
  ) { }

  @ViewChild('roleInput') roleInput! : ElementRef;

  form! : FormGroup
  fd = new FormData()
  isSaved = false
  initialFormValue : any

  jobCategorySectionData : any
  roles : any[] = []
  selectedRoles : any;

  ngOnInit(): void {
    this.getJobCategorySectionData();

    this.form = this.fb.nonNullable.group({
      smallHeading : ["", [Validators.required]],
      mainHeading : ["", [Validators.required]]
    })
  }

  getJobCategorySectionData() {
    this.service.getJobCategorySectionData().subscribe((res) => {
      this.jobCategorySectionData = res.data;
      
      this.service.listJobCategorySectionItems().subscribe((res) => {
        this.roles = res.data;
        this.selectedRoles = this.roles.filter(role => role.isSelected).length;

        this.form.patchValue({
          smallHeading : this.jobCategorySectionData?.smallHeading,
          mainHeading : this.jobCategorySectionData?.mainHeading
        })
        this.roles = res.data;

        this.initialFormValue = this.form.value;

      })
    })
  }

  onSubmit() {
    const data = {
      ... this.form.value,
      roles : this.roles
        .filter(role => role.isSelected)
        .map(role => role._id)
    }

    this.service.addJobCategorySectionData(data).subscribe((res) => {
      console.log("job category section data posted!");
      this.isSaved = true;
    })
  }


  addRole(newRole : string) {
      if(newRole.trim() !== ''){
        this.service.addJobCategorySectionItem({"role" : newRole}).subscribe((res) => {
          this.getJobCategorySectionData();
        })
        this.roleInput.nativeElement.value = '';
      }
  }

  selectRole(id:any) {
    this.service.selectJobCategorySectionItem(id).subscribe((res) => {
        this.getJobCategorySectionData();
    })
  }

  isFormUnchanged(): boolean {
    return JSON.stringify(this.form.value) === JSON.stringify(this.initialFormValue);
  }

  async next() {
    if(!this.isFormUnchanged() && !this.isSaved){
      const modal = this.modal.open(WarningModalComponent, {
        size : "lg"
      })

      const res = await modal.result;

      if(res){
        this.router.navigate(["/workforce-admin/landing-page/pricing"]);
      }
    }else{
        this.router.navigate(["/workforce-admin/landing-page/pricing"]);
    }
  }

  async previous() {
    if(!this.isFormUnchanged() && !this.isSaved){
      const modal = this.modal.open(WarningModalComponent, {
        size : "lg"
      })

      const res = await modal.result;

      if(res){
        this.router.navigate(["/workforce-admin/landing-page/steps"]);
      }
    }else{
        this.router.navigate(["/workforce-admin/landing-page/steps"]);
    }
  }

}
