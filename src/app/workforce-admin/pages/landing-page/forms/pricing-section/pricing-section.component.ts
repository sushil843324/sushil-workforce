import { Component, OnInit } from '@angular/core';
import { LandingPageService } from '../../services/landing-page.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WarningModalComponent } from '../../components/warning-modal/warning-modal.component';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-pricing-section',
  templateUrl: './pricing-section.component.html',
  styleUrls: ['./pricing-section.component.scss']
})
export class PricingSectionComponent implements OnInit {

  constructor(
    private service : LandingPageService,
    private fb : FormBuilder,
    private router : Router,
    private modal : NgbModal
  ) { }
  
  form! : FormGroup
  budgetForm! : FormGroup
  pricingSectionData : any
  budgets : any[] = []
  selectedBudgets : any
  isSaved = false
  initialFormValue : any

  ngOnInit(): void {
    this.getPricingSectionData();

    this.form = this.fb.nonNullable.group({
      smallHeading : ["", [Validators.required]],
      mainHeading : ["", [Validators.required]]
    })
    this.budgetForm = this.fb.nonNullable.group({
      role : ["", [Validators.required]],
      budget : ["", [Validators.required]]
    })
  }

  getPricingSectionData() {
    this.service.getPricingSectionData().subscribe((res) => {
      this.pricingSectionData = res.data;

      this.service.getBudgetItems().subscribe((res) => {
        this.budgets = res.data;
        this.selectedBudgets = this.budgets.filter(item => item.isSelected).length;
      })


      this.form.patchValue({
        smallHeading : this.pricingSectionData?.smallHeading,
        mainHeading : this.pricingSectionData?.mainHeading
      })

      this.initialFormValue = this.form.value;

      
    })
  }

  onSubmit() {
    const data = {
      ... this.form.value,
      budgets : this.budgets
                      .filter(item => item.isSelected)
                      .map(item => item._id)
    }

      this.service.addPricingSectionData(data).subscribe((res) => {
        console.log("pricing section data added!");
        this.isSaved = true;
        this.getPricingSectionData();
      })
  }

  selectBudget(id : any) {
    this.service.selectBudgetItem(id).subscribe((res) => {
        console.log("budget item selected!");
        this.getPricingSectionData();
    })
  }

  addBudget() {
    this.service.addBudgetItem(this.budgetForm.value).subscribe((res) => {
      console.log("budget item added!");
      this.getPricingSectionData();
    })
  }

  isFormUnchanged(): boolean {
    return JSON.stringify(this.form.value) === JSON.stringify(this.initialFormValue);
  }

  async next() {
    if(!this.isFormUnchanged() && !this.isSaved){
      const modal = this.modal.open(WarningModalComponent, {
        size : "lg"
      })

      const res = await modal.result;

      if(res){
        this.router.navigate(["/workforce-admin/landing-page/testimonials"]);
      }
    }else{
        this.router.navigate(["/workforce-admin/landing-page/testimonials"]);
    }
  }

  async previous() {
    if(!this.isFormUnchanged() && !this.isSaved){
      const modal = this.modal.open(WarningModalComponent, {
        size : "lg"
      })

      const res = await modal.result;

      if(res){
        this.router.navigate(["/workforce-admin/landing-page/steps"]);
      }
    }else{
        this.router.navigate(["/workforce-admin/landing-page/steps"]);
    }
  }

  deleteItem(id:any){
    this.service.deleteBudgetItem(id).subscribe((res) => {
      console.log("budget item deleted!");
      this.getPricingSectionData();
    })
    
  }



}
