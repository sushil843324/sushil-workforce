import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SlidingLogosSectionComponent } from './sliding-logos-section.component';

describe('SlidingLogosSectionComponent', () => {
  let component: SlidingLogosSectionComponent;
  let fixture: ComponentFixture<SlidingLogosSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SlidingLogosSectionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SlidingLogosSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
