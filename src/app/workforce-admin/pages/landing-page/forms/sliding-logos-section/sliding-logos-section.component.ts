import { Component, OnInit } from '@angular/core';
import { LandingPageService } from '../../services/landing-page.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadFileModalComponent } from './upload-file-modal/upload-file-modal.component';
import { WarningModalComponent } from '../../components/warning-modal/warning-modal.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sliding-logos-section',
  templateUrl: './sliding-logos-section.component.html',
  styleUrls: ['./sliding-logos-section.component.scss']
})
export class SlidingLogosSectionComponent implements OnInit {

  constructor(
    private service : LandingPageService,
    private modal : NgbModal,
    private router : Router
  ) { }

  fd = new FormData()
  logos : any
  isSaved = false

  ngOnInit(): void {
    this.getSlidingLogosSectionData();
  }

  async openModal() {
    const modal = this.modal.open(UploadFileModalComponent,{
      size : "lg"
    });
    
    const result = await modal.result;
    if(result){
      this.getSlidingLogosSectionData();
    }
  }

  getSlidingLogosSectionData() {
    this.service.getSlidingLogoSectionData().subscribe((res) => {
      this.logos = res.data;
    })
  }

  deleteLogo(id:any) {
    this.service.deleteSlidingLogo(id).subscribe((res) => {
      console.log("logo deleted!");
      this.getSlidingLogosSectionData();
    })
  }

}
