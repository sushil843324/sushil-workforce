import { Component, OnInit } from '@angular/core';
import { LandingPageService } from '../../../services/landing-page.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-upload-file-modal',
  templateUrl: './upload-file-modal.component.html',
  styleUrls: ['./upload-file-modal.component.scss']
})
export class UploadFileModalComponent implements OnInit {

  constructor(
    private service : LandingPageService,
    private activeModal : NgbActiveModal
  ) { }

  fd = new FormData()
  selectedLogo : any

  ngOnInit(): void {
  }

  selectFile(event : any) {
    const file = event.target.files[0];
    this.fd.append('files', file);
    this.selectedLogo = file;
    console.log(file, 'here');
    
  }

  addLogo() {
    this.service.addSlidingLogoSectionData(this.fd).subscribe((res) => {
      console.log("logo added!");
      this.activeModal.close(true);
  })
  }

}
