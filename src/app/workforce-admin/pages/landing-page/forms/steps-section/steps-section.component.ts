import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LandingPageService } from '../../services/landing-page.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { WarningModalComponent } from '../../components/warning-modal/warning-modal.component';

@Component({
  selector: 'app-steps-section',
  templateUrl: './steps-section.component.html',
  styleUrls: ['./steps-section.component.scss']
})
export class StepsSectionComponent implements OnInit {

  constructor(
    private fb : FormBuilder,
    private service : LandingPageService,
    private router : Router,
    private modal : NgbModal
  ) { }

  form! : FormGroup
  fd = new FormData()

  stepSectionData : any
  isSaved = false
  initialFormValue : any

  ngOnInit(): void {
    this.getStepSectionData();

    this.form = this.fb.nonNullable.group({
      smallHeading : ["", [Validators.required]],
      mainHeading: ["", [Validators.required]],
      step1 : this.fb.nonNullable.group({
        heading : ["", [Validators.required]],
        description : ["", [Validators.required]]
      }),
      step2 : this.fb.nonNullable.group({
        heading : ["", [Validators.required]],
        description : ["", [Validators.required]]
      }),
      step3 : this.fb.nonNullable.group({
        heading : ["", [Validators.required]],
        description : ["", [Validators.required]]
      })
    })
  }

  getStepSectionData() {
    this.service.getStepsSectionData().subscribe((res) => {
      this.stepSectionData = res.data;

      this.form.patchValue({
        smallHeading : this.stepSectionData?.smallHeading,
        mainHeading : this.stepSectionData?.mainHeading,
        step1 : this.stepSectionData?.step1,
        step2 : this.stepSectionData?.step2,
        step3 : this.stepSectionData?.step3
      })
      this.initialFormValue = this.form.value;
    })
  }

  onSubmit() {
    
    this.service.addStepsSectionData(this.form.value).subscribe((res) => {
      console.log("Step section data posted!");
      this.isSaved = true;
    })
  }

  isFormUnchanged(): boolean {
    return JSON.stringify(this.form.value) === JSON.stringify(this.initialFormValue);
  } 
  async next() {
    if(!this.isFormUnchanged() && !this.isSaved){
      const modal = this.modal.open(WarningModalComponent, {
        size : "lg"
      })

      const res = await modal.result;

      if(res){
        this.router.navigate(["/workforce-admin/landing-page/job-categories"]);
      }
    }else{
        this.router.navigate(["/workforce-admin/landing-page/job-categories"]);
    }
  }

  async previous() {
    if(!this.isFormUnchanged() && !this.isSaved){
      const modal = this.modal.open(WarningModalComponent, {
        size : "lg"
      })

      const res = await modal.result;

      if(res){
        this.router.navigate(["/workforce-admin/landing-page/features"]);
      }
    }else{
        this.router.navigate(["/workforce-admin/landing-page/features"]);
    }
  }

}
