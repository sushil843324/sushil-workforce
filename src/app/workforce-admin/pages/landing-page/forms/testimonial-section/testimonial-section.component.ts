import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { LandingPageService } from '../../services/landing-page.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WarningModalComponent } from '../../components/warning-modal/warning-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-testimonial-section',
  templateUrl: './testimonial-section.component.html',
  styleUrls: ['./testimonial-section.component.scss']
})
export class TestimonialSectionComponent implements OnInit {

  constructor(
    private service : LandingPageService,
    private fb : FormBuilder,
    private modal : NgbModal,
    private router : Router
  ) { }

  @ViewChild('testimonialSelect') testimonialSelect!: ElementRef;

  form! : FormGroup
  publishedTestimonials : any
  testimonialSectionData : any
  selectedTestimonials : any[] = []
  isSaved = false
  initialFormValue : any

  ngOnInit(): void {
    this.getTestimonialSectionData();

    this.form = this.fb.nonNullable.group({
      heading : ["", [Validators.required]],
      description : ["", [Validators.required]]
    })
  }

  getTestimonialSectionData() {
    this.service.getTestimonialSectionData().subscribe((res) => {
      console.log("testimonial section data fetched");
      this.testimonialSectionData = res.data;

      // Fetch published testimonials that are not selected
      this.service.getPublishedTestimonialsList().subscribe((res) => {
        this.publishedTestimonials = res.data
                            .filter((item:any) => {
                                return (
                                  !(this.selectedTestimonials?.map((m:any) => m.testimonial_id)).includes(item._id)
                                )
                            });
      })

      // Fetch the selected testimonials
      this.service.getTestimonialItemsList().subscribe((res) => {
        this.selectedTestimonials = res.data;
      })

      this.form.patchValue({
        heading : this.testimonialSectionData?.heading,
        description : this.testimonialSectionData?.description
      })
      this.initialFormValue = this.form.value;

    })
  }

  deleteTestimonial(id:any) {
    this.service.deleteTestimonialItem(id).subscribe((res) => {
      console.log("testimonial removed!");
      this.getTestimonialSectionData();
    })
  }

  selectTestimonial(id:any) {
    console.log(id, 'here');
    
    this.service.addTestimonialItem(id).subscribe((res) => {
      console.log("testimonial item added");
      this.getTestimonialSectionData();
    })
  }

  onSubmit() {
      let data = this.form.value;
      if(this.selectedTestimonials && this.selectedTestimonials.length > 0){
        data = {
          ...this.form.value,
          testimonials : this.selectedTestimonials.
                            map((item:any) => item._id)
        }
      }

      this.service.addTestimonialSectionData(data).subscribe((res) => {
        console.log("testimonial section data added");
        this.isSaved = true;
        this.getTestimonialSectionData();
      })
  }

  isFormUnchanged(): boolean {
    return JSON.stringify(this.form.value) === JSON.stringify(this.initialFormValue);
  }


  async previous() {
    if(!this.isFormUnchanged() && !this.isSaved){
      const modal = this.modal.open(WarningModalComponent, {
        size : "lg"
      })

      const res = await modal.result;

      if(res){
        this.router.navigate(["/workforce-admin/landing-page/pricing"]);
      }
    }else{
        this.router.navigate(["/workforce-admin/landing-page/pricing"]);
    }
  }



}
