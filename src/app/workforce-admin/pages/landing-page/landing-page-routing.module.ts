import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { BannerSectionComponent } from './forms/banner-section/banner-section.component';
import { FeaturesSectionComponent } from './forms/features-section/features-section.component';
import { JobCategorySectionComponent } from './forms/job-category-section/job-category-section.component';
import { PricingSectionComponent } from './forms/pricing-section/pricing-section.component';
import { SlidingLogosSectionComponent } from './forms/sliding-logos-section/sliding-logos-section.component';
import { StepsSectionComponent } from './forms/steps-section/steps-section.component';
import { TestimonialSectionComponent } from './forms/testimonial-section/testimonial-section.component';

const routes: Routes = [
  {
    path : "",
    component : MainLayoutComponent,
    children : [
      {
        path : "",
        component : BannerSectionComponent
      },
      {
        path : "features",
        component : FeaturesSectionComponent
      },
      {
        path : "job-categories",
        component : JobCategorySectionComponent
      },
      {
        path : "pricing",
        component : PricingSectionComponent
      },
      {
        path : "sliding-logos",
        component : SlidingLogosSectionComponent
      },
      {
        path : "steps",
        component : StepsSectionComponent
      },
      {
        path : "testimonials",
        component : TestimonialSectionComponent
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingPageRoutingModule { }
