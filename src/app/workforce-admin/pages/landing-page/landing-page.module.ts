import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingPageRoutingModule } from './landing-page-routing.module';
import { BannerSectionComponent } from './forms/banner-section/banner-section.component';
import { SlidingLogosSectionComponent } from './forms/sliding-logos-section/sliding-logos-section.component';
import { FeaturesSectionComponent } from './forms/features-section/features-section.component';
import { StepsSectionComponent } from './forms/steps-section/steps-section.component';
import { JobCategorySectionComponent } from './forms/job-category-section/job-category-section.component';
import { PricingSectionComponent } from './forms/pricing-section/pricing-section.component';
import { TestimonialSectionComponent } from './forms/testimonial-section/testimonial-section.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { ContactFormComponent } from './pages/components/contact-form/contact-form.component';
import { TestimonialsListPageComponent } from './pages/testimonials-list-page/testimonials-list-page.component';
import { PricingListPageComponent } from './pages/pricing-list-page/pricing-list-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UploadFileModalComponent } from './forms/sliding-logos-section/upload-file-modal/upload-file-modal.component';
import { WarningModalComponent } from './components/warning-modal/warning-modal.component';


@NgModule({
  declarations: [
    BannerSectionComponent,
    SlidingLogosSectionComponent,
    FeaturesSectionComponent,
    StepsSectionComponent,
    JobCategorySectionComponent,
    PricingSectionComponent,
    TestimonialSectionComponent,
    MainLayoutComponent,
    ContactFormComponent,
    TestimonialsListPageComponent,
    PricingListPageComponent,
    UploadFileModalComponent,
    WarningModalComponent
  ],
  imports: [
    CommonModule,
    LandingPageRoutingModule,
    ReactiveFormsModule
  ]
})
export class LandingPageModule { }
