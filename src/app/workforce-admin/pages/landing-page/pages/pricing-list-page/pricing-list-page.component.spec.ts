import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PricingListPageComponent } from './pricing-list-page.component';

describe('PricingListPageComponent', () => {
  let component: PricingListPageComponent;
  let fixture: ComponentFixture<PricingListPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PricingListPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PricingListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
