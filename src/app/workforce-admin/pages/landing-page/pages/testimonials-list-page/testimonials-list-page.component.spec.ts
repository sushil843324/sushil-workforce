import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestimonialsListPageComponent } from './testimonials-list-page.component';

describe('TestimonialsListPageComponent', () => {
  let component: TestimonialsListPageComponent;
  let fixture: ComponentFixture<TestimonialsListPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestimonialsListPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestimonialsListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
