import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from 'src/app/workforce-admin/interfaces/apiResponse';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LandingPageService {

  constructor(
    private http : HttpClient
  ) { }

  // Banner Section

  getBannerSectionData() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/banner-section/get`
    )
  }

  addBannerSectionData(payload:any) {
    return this.http.post<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/banner-section/add`,
      payload
    )
  }

  // Sliding logo section

  getSlidingLogoSectionData() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/sliding-logo-section/get`
    )
  }

  addSlidingLogoSectionData(payload:any) {
    return this.http.post<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/sliding-logo-section/add`,
      payload
    )
  }

  deleteSlidingLogo(id : any) {
    return this.http.delete<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/sliding-logo-section/delete?logoId=${id}`
    )
  }

  // Features Section

  getFeaturesSectionData() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/feature-section/get`
    )
  }

  addFeaturesSectionData(payload:any) {
    return this.http.post<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/feature-section/add`,
      payload
    )
  }


  // Steps section

  getStepsSectionData() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/steps-section/get`
    )
  }

  addStepsSectionData(payload:any) {
    return this.http.post<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/steps-section/add`,
      payload
    )
  }

  // Job Category Section

  getJobCategorySectionData() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/job-category-section/get`
    )
  }

  addJobCategorySectionData(payload:any) {
    return this.http.post<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/job-category-section/add`,
      payload
    )
  }

  addJobCategorySectionItem(item : any) {
    return this.http.post<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/job-category-section/item/add`,
      item
    )
  }

  listJobCategorySectionItems() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/job-category-section/item/list`
    )
  }
  
  selectJobCategorySectionItem(id:any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/job-category-section/item/select?itemId=${id}`
    )
  }

  // Pricing Section

  getPricingSectionData() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/pricing-section/get`
    )
  }

  addPricingSectionData(payload:any) {
    return this.http.post<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/pricing-section/add`,
      payload
    )
  }

  getBudgetItems() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/pricing-section/item/list`
    )
  }

  addBudgetItem(payload:any) {
    return this.http.post<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/pricing-section/item/add`,
      payload
    )
  }

  selectBudgetItem(id:any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/pricing-section/item/select?itemId=${id}`
    )
  }

  deleteBudgetItem(id:any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/pricing-section/item/delete?itemId=${id}`
    )
  }

  // Testimonial Section

  getTestimonialSectionData() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/testimonial-section/get`
    )
  }

  
  addTestimonialSectionData(payload:any) {
    return this.http.post<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/testimonial-section/add`,
      payload
      )
    }

    getTestimonialItemsList() {
      return this.http.get<ApiResponse>(
        `${environment.workforceUrl}/admin/landing-page/testimonial-section/item/list`
      )
    }

    deleteTestimonialItem(id:any) {
      return this.http.delete<ApiResponse>(
        `${environment.workforceUrl}/admin/landing-page/testimonial-section/item/delete?testimonialId=${id}`
      )
    }
    
  addTestimonialItem(id:any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/testimonial-section/item/add?testimonialId=${id}`
    )
  }

  getPublishedTestimonialsList() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/feedbacks/list?isPublished=true`
    )    
  }





}
