import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectRequestsService } from '../../services/project-requests.service';

@Component({
  selector: 'app-edit-project-modal',
  templateUrl: './edit-project-modal.component.html',
  styleUrls: ['./edit-project-modal.component.scss']
})
export class EditProjectModalComponent implements OnInit {
  
  @Input() projectDetail : any;


  constructor(
    private fb : FormBuilder,
    private activeModal : NgbActiveModal,
    private service : ProjectRequestsService
  ) { }

  @ViewChild('selectElement') selectElement!: ElementRef;

  form! : FormGroup
  fd = new FormData()
  scopeOfWorkFiles : any
  selectedSkills : any[] = [] 
  isFileCheckBoxTicked = false
  isSubmitted = false

  skills = [
    "HTML", "CSS", "JavaScript", "NodeJs", "TypeScript", "AWS", "NextJs", "Angular", "Vue", "QA Testing", "Network Engineering"
  ]

  ngOnInit(): void {
    
    this.form = this.fb.nonNullable.group({
      job_title : ["", [Validators.required]],
      job_deadline : ["", [Validators.required]],
      job_location : ["", [Validators.required]],
      job_duration : ["", [Validators.required]],
      job_skills : [[], [Validators.required]],
      job_additional_info : ["", [Validators.required]],
      job_scope_of_work_files : [[], [Validators.required]],
      job_stipend : ["", [Validators.required]],
      stipend_reduction_percentage : [0, [Validators.required]]
    })

    this.form.patchValue({
      job_title : this.projectDetail?.job_title,
      job_location : this.projectDetail?.job_location,
      job_deadline : this.projectDetail?.job_deadline,
      job_duration : this.projectDetail?.job_duration,
      job_stipend : this.projectDetail?.job_stipend,
      job_skills : this.projectDetail?.job_skills,
      job_scope_of_work_files : this.projectDetail?.job_scope_of_work_files
    })
  }


  selectedFiles(event : any) {
    this.scopeOfWorkFiles = event.target.files;
        
    for(let file of this.scopeOfWorkFiles) {
      this.fd.append("files", file);
    }
  }

  selectSkill(event:any) {
    if(!this.projectDetail?.job_skills.includes(event.target.value))
        this.selectedSkills.push(event.target.value);

    this.selectElement.nativeElement.selectedIndex = 0;
    console.log(this.selectedSkills, "here");
    
  }

  toggleCheckBox(): void {
    this.isFileCheckBoxTicked = !this.isFileCheckBoxTicked;
  }

  onSubmit() {
    this.isSubmitted = true;
    this.form.patchValue({
      job_skills : [...this.projectDetail?.job_skills || [],  ...this.selectedSkills]
    })

    Object.keys(this.form.value).forEach((key) => {
      if(key == "job_skills" || key=="job_scope_of_work_files")
        this.fd.append(key, JSON.stringify(this.form.value[key]));
      else
      this.fd.append(key, this.form.value[key]);
    })

    console.log(this.form.value, "here");
    

    this.service.editAndPublishProject( this.projectDetail._id ,this.fd).subscribe((res) => {
      console.log("project published successfully!");
      this.fd = new FormData();
      this.activeModal.close(true);
    })

  }

  closeModal() {
    this.activeModal.close(false);
  }

}
