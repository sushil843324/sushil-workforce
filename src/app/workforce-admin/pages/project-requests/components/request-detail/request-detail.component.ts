import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { ProjectRequestsService } from '../../services/project-requests.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditProjectModalComponent } from '../edit-project-modal/edit-project-modal.component';

@Component({
  selector: 'app-request-detail',
  templateUrl: './request-detail.component.html',
  styleUrls: ['./request-detail.component.scss']
})
export class RequestDetailComponent implements OnInit {

  constructor(
    private route : ActivatedRoute,
    private router : Router,
    private service : ProjectRequestsService,
    private modal : NgbModal
  ) { }

  projectId : any;
  projectDetail : any;
  organizationDetail : any;
  organizationId : any;
  otherJobs : any;
  isProjectPublished = false;

  ngOnInit(): void {
    this.ProjectDetail();
  }


  ProjectDetail() {
    this.projectId = this.route.snapshot.params["id"];
    
    this.service.getRequestDetail(this.projectId).subscribe((res) => {
      this.projectDetail = res.data[0];

      this.isProjectPublished = res.data[0]?.request_status == "published";

      this.organizationDetail = res.data[0]?.company_data[0];
      this.organizationId = this.organizationDetail?._id;
      
      this.listOtherProjects(this.organizationId);
    })
  }

  listOtherProjects(id : any) {
    this.service.getRequestsList(id).subscribe((res) => {
      this.otherJobs = res.data.filter((project : any) => project._id !== this.projectId);
    })
  }

  navigateToProject(projectId : any) {
    console.log("clicked ");
    
    this.router.navigate(['/workforce-admin/project-requests/detail/', projectId]);
    
    this.ProjectDetail();
    // this.service.getRequestDetail(projectId).subscribe((res) => {
    //   this.projectDetail = res.data[0];
    // })
  }

  async openModal() {
    if(!this.isProjectPublished) {
      const modal = this.modal.open(EditProjectModalComponent, {
        size : "xl"
      })
  
      modal.componentInstance.projectDetail = this.projectDetail;
  
      const result = await modal.result;
      if(result) {
        this.service.publishProject(this.projectId).subscribe((res) => {
          console.log("project published successfully");
          this.ProjectDetail();
        })
      }
    }

  }



}
