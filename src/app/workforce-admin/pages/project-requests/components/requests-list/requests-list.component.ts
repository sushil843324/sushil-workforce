import { Component, OnInit } from '@angular/core';
import { ProjectRequestsService } from '../../services/project-requests.service';

@Component({
  selector: 'app-requests-list',
  templateUrl: './requests-list.component.html',
  styleUrls: ['./requests-list.component.scss']
})
export class RequestsListComponent implements OnInit {

  constructor(
    private service : ProjectRequestsService
  ) { }

  requestsList : any
  sort = "latest"

  ngOnInit(): void {
    this.listProjectRequests();
  }

  listProjectRequests() {
    this.service.getRequestsList(this.sort).subscribe((res) => {
      this.requestsList = res.data;
    })
  }

  sortList(event:any) {
    this.sort = event.target.value;
    this.listProjectRequests();
  }

}
