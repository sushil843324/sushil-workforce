import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RequestsListComponent } from './components/requests-list/requests-list.component';
import { RequestDetailComponent } from './components/request-detail/request-detail.component';

const routes: Routes = [
  {
    path : "list",
    component : RequestsListComponent
  },
  {
    path : "detail/:id",
    component : RequestDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRequestsRoutingModule { }
