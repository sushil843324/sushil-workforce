import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectRequestsRoutingModule } from './project-requests-routing.module';
import { OrganizationDetailCardComponent } from './components/organization-detail-card/organization-detail-card.component';
import { EditProjectModalComponent } from './components/edit-project-modal/edit-project-modal.component';
import { RequestDetailComponent } from './components/request-detail/request-detail.component';
import { RequestsListComponent } from './components/requests-list/requests-list.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    OrganizationDetailCardComponent,
    EditProjectModalComponent,
    RequestDetailComponent,
    RequestsListComponent
  ],
  imports: [
    CommonModule,
    ProjectRequestsRoutingModule,
    ReactiveFormsModule
  ]
})
export class ProjectRequestsModule { }
