import { TestBed } from '@angular/core/testing';

import { ProjectRequestsService } from './project-requests.service';

describe('ProjectRequestsService', () => {
  let service: ProjectRequestsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProjectRequestsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
