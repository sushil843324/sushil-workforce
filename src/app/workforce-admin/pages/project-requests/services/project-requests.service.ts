import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from 'src/app/workforce-admin/interfaces/apiResponse';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectRequestsService {

  constructor(
    private http : HttpClient
  ) { }

  // GET

  getRequestsList(sort:any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/jobs/requests/list?sort=${sort}`
    )
  }

  getRequestDetail(projectId : any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/jobs/requests/get?jobId=${projectId}`
    )
  }

  getOrganizationDetail(organizationId : any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/recruiter/organization/get?organizationId=${organizationId}`
    )
  }

  publishProject(projectId : any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/jobs/requests/change-status?jobId=${projectId}`
    )
  }


  // POST

  editAndPublishProject(projectId : any, payload : any) {
    return this.http.post(
      `${environment.workforceUrl}/admin/jobs/requests/edit-before-publish?jobId=${projectId}`,
      payload
    )
  }




}
