import { Component, OnInit } from '@angular/core';
import { ProposalsService } from '../../services/proposals.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadFileModalComponent } from '../upload-file-modal/upload-file-modal.component';

@Component({
  selector: 'app-proposal-list',
  templateUrl: './proposal-list.component.html',
  styleUrls: ['./proposal-list.component.scss']
})
export class ProposalListComponent implements OnInit {

  constructor(
    private service : ProposalsService,
    private modal : NgbModal
  ) { }

  proposals : any
  companies : any

  ngOnInit(): void {
    this.listProposals();
    this.listCompanies();
  }

  listCompanies() {
    this.service.getCompanyList().subscribe((res) => {
      this.companies = res.data;
    })
  }

  listProposals() {
    this.service.getProposalsList("").subscribe((res) => {
      this.proposals = res.data;
    })
  }

  async uploadNewProposal() {
    const modal = this.modal.open(UploadFileModalComponent, {
      size : "lg"
    });

    modal.componentInstance.companies = this.companies;

    const result = await modal.result;
    if(result)
      this.listProposals();

  }

}
