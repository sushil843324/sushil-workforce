import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { ProposalsService } from '../../services/proposals.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-upload-file-modal',
  templateUrl: './upload-file-modal.component.html',
  styleUrls: ['./upload-file-modal.component.scss']
})
export class UploadFileModalComponent implements OnInit {

  constructor(
    private service : ProposalsService,
    private fb : FormBuilder,
    private activeModal : NgbActiveModal
  ) { }

  form! : FormGroup

  @ViewChild('companySelect') companySelect! : ElementRef;

  @Input() companies! : any

  fd = new FormData()
  selectedCompany : any
  projects : any
  selectedProject : any
  proposalFile : any

  ngOnInit(): void {
    this.form = this.fb.nonNullable.group({
      companyId : ["", [Validators.required]],
      jobId : ["", [Validators.required]],
      remarks : ["", [Validators.required]]
    })
  }


  fetchProjects(id : any) {
    this.service.getProjectsList(id).subscribe((res) => {
      this.projects = res.data;
      console.log(this.projects, "hesrsr");
      
    })
  }

  selectFiles(event : any) {
    const file = event.target.files[0];
    console.log(file, 'here if file');
    
    this.proposalFile = file;
    this.fd.append("proposal_file", file)
  }

  onSubmit() {

    Object.keys(this.form.value).forEach((key) => {
      this.fd.append(key, this.form.value[key])
    })

    this.service.addProposal(this.fd).subscribe((res) => {
      console.log("proposal added!");
      this.activeModal.close(true);
    })
  }

  closeModal() {
    this.activeModal.close(true);
  }

  deleteFile() {
    this.proposalFile = null;
  }


}
