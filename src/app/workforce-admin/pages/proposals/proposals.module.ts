import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProposalsRoutingModule } from './proposals-routing.module';
import { UploadFileModalComponent } from './components/upload-file-modal/upload-file-modal.component';
import { ProposalListComponent } from './components/proposal-list/proposal-list.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    UploadFileModalComponent,
    ProposalListComponent
  ],
  imports: [
    CommonModule,
    ProposalsRoutingModule,
    ReactiveFormsModule
  ]
})
export class ProposalsModule { }
