import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from 'src/app/organization/interfaces/organization-types';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProposalsService {

  constructor(
    private http : HttpClient
  ) { }

  // GET

  getProposalsList(order : any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/proposals/list?sort=${order}`
    )
  }

  getCompanyList() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/organizations/list`
    )
  }

  getProjectsList(id:any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/jobs/requests/list?organizationId=${id}`
    )
  }

  // POST

  addProposal(payload : any) {
    return this.http.post<ApiResponse>(
      `${environment.workforceUrl}/admin/proposals/add`,
      payload
    )
  }
  

}
