import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationDetailCardComponent } from './organization-detail-card.component';

describe('OrganizationDetailCardComponent', () => {
  let component: OrganizationDetailCardComponent;
  let fixture: ComponentFixture<OrganizationDetailCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizationDetailCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrganizationDetailCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
