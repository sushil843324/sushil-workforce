import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-organization-detail-card',
  templateUrl: './organization-detail-card.component.html',
  styleUrls: ['./organization-detail-card.component.scss']
})
export class OrganizationDetailCardComponent implements OnInit {

  constructor() { }

  @Input() organizationDetail : any;
  isReadMore = false;

  ngOnInit(): void {
  }

}
