import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishedProjectDetailComponent } from './published-project-detail.component';

describe('PublishedProjectDetailComponent', () => {
  let component: PublishedProjectDetailComponent;
  let fixture: ComponentFixture<PublishedProjectDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublishedProjectDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PublishedProjectDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
