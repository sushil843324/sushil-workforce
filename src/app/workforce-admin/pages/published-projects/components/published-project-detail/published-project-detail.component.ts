import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PublishedProjectService } from '../../services/published-project.service';

@Component({
  selector: 'app-published-project-detail',
  templateUrl: './published-project-detail.component.html',
  styleUrls: ['./published-project-detail.component.scss']
})
export class PublishedProjectDetailComponent implements OnInit {

  constructor(
    private route : ActivatedRoute,
    private service : PublishedProjectService
  ) { }

  projectId : any;
  projectDetails : any;
  organizationDetail : any;
  applications : any;

  ngOnInit(): void {
    this.projectId = this.route.snapshot.params["id"];
    this.PublishedProjectDetails();
    this.listApplications();
  }

  PublishedProjectDetails() {
    this.service.getPublishedProjectDetail(this.projectId).subscribe((res) => {
      this.projectDetails = res.data[0];
      this.organizationDetail = res.data[0]?.company_data[0];
    })
  }

  toggleCurrentStatus(projectId : any) {
    this.service.toggleCurrentStatus(projectId).subscribe((res) => {
      console.log("current status changed!");
      this.PublishedProjectDetails();
    })
  }

  listApplications() {
    this.service.getApplicationsList(this.projectId).subscribe((res) => {
      this.applications = res.data;
    })
  }

}
