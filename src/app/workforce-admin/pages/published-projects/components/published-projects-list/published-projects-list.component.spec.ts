import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishedProjectsListComponent } from './published-projects-list.component';

describe('PublishedProjectsListComponent', () => {
  let component: PublishedProjectsListComponent;
  let fixture: ComponentFixture<PublishedProjectsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublishedProjectsListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PublishedProjectsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
