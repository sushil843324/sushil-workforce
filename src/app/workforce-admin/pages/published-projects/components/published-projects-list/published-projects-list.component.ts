import { Component, OnInit } from '@angular/core';
import { PublishedProjectService } from '../../services/published-project.service';

@Component({
  selector: 'app-published-projects-list',
  templateUrl: './published-projects-list.component.html',
  styleUrls: ['./published-projects-list.component.scss']
})
export class PublishedProjectsListComponent implements OnInit {

  constructor(
    private service : PublishedProjectService
  ) { }

  publishedProjectsList : any;
  

  ngOnInit(): void {
    this.listPublishedProjects();
  }

  listPublishedProjects() {
    this.service.getPublishedProjectsList().subscribe((res) => {
      this.publishedProjectsList = res.data;
    })
  }

}
