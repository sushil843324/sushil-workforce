import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublishedProjectsListComponent } from './components/published-projects-list/published-projects-list.component';
import { PublishedProjectDetailComponent } from './components/published-project-detail/published-project-detail.component';

const routes: Routes = [
  {
    path : "list",
    component : PublishedProjectsListComponent
  },
  {
    path : "detail/:id",
    component : PublishedProjectDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublishedProjectsRoutingModule { }
