import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublishedProjectsRoutingModule } from './published-projects-routing.module';
import { PublishedProjectsListComponent } from './components/published-projects-list/published-projects-list.component';
import { PublishedProjectDetailComponent } from './components/published-project-detail/published-project-detail.component';
import { OrganizationDetailCardComponent } from './components/organization-detail-card/organization-detail-card.component';


@NgModule({
  declarations: [
    PublishedProjectsListComponent,
    PublishedProjectDetailComponent,
    OrganizationDetailCardComponent
  ],
  imports: [
    CommonModule,
    PublishedProjectsRoutingModule
  ]
})
export class PublishedProjectsModule { }
