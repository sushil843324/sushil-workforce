import { TestBed } from '@angular/core/testing';

import { PublishedProjectService } from './published-project.service';

describe('PublishedProjectService', () => {
  let service: PublishedProjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PublishedProjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
