import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from 'src/app/organization/interfaces/organization-types';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PublishedProjectService {

  constructor(
    private http : HttpClient
  ) { }


  // GET

  getPublishedProjectsList() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/jobs/published/list`
    )
  }

  getPublishedProjectDetail(projectId:any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/jobs/published/get?jobId=${projectId}`
    )
  }

  toggleCurrentStatus(projectId : any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/jobs/published/reactivate?jobId=${projectId}`
    )
  }

  getApplicationsList(projectId : any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/jobs/applications/list?jobId=${projectId}`
    )
  }



}
