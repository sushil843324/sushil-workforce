import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishedTestimonialsListComponent } from './published-testimonials-list.component';

describe('PublishedTestimonialsListComponent', () => {
  let component: PublishedTestimonialsListComponent;
  let fixture: ComponentFixture<PublishedTestimonialsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublishedTestimonialsListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PublishedTestimonialsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
