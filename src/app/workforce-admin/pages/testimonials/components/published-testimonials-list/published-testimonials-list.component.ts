import { Component, OnInit } from '@angular/core';
import { TestimonialsService } from '../../services/testimonials.service';
import { ViewTestimonialModalComponent } from '../view-testimonial-modal/view-testimonial-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-published-testimonials-list',
  templateUrl: './published-testimonials-list.component.html',
  styleUrls: ['./published-testimonials-list.component.scss']
})
export class PublishedTestimonialsListComponent implements OnInit {

  constructor(
    private service : TestimonialsService,
    private modal : NgbModal
  ) { }

  publishedTestimonials : any
  sort = "latest"

  ngOnInit(): void {
    this.listPublishedTestimonials();
  }


  listPublishedTestimonials() {
    this.service.getTestimonialsList(true, this.sort).subscribe((res) => {
      this.publishedTestimonials = res.data;
    })
  }

  unPublishTestimonial(id:any) {
    this.service.publishTestimonial(id).subscribe((res) => {
      this.listPublishedTestimonials();
    })
  }

  openTestimonialModal(id : any) {
    const modal = this.modal.open(ViewTestimonialModalComponent, {
      size : "xl"
    });

    modal.componentInstance.testimonialId = id;

  }

  sortList(event:any) {
    this.sort = event.target.value;
    this.listPublishedTestimonials();
  }

}
