import { Component, OnInit } from '@angular/core';
import { TestimonialsService } from '../../services/testimonials.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewTestimonialModalComponent } from '../view-testimonial-modal/view-testimonial-modal.component';

@Component({
  selector: 'app-testimonials-list',
  templateUrl: './testimonials-list.component.html',
  styleUrls: ['./testimonials-list.component.scss']
})
export class TestimonialsListComponent implements OnInit {

  constructor(
    private service : TestimonialsService,
    private modal : NgbModal
  ) { }

  testimonials : any
  sort = "latest"

  ngOnInit(): void {
    this.listTestimonials();
  }

  listTestimonials() {
    this.service.getTestimonialsList(false, this.sort).subscribe((res) => {
      this.testimonials = res.data;
    })
  }

  openTestimonialModal(id : any) {
    const modal = this.modal.open(ViewTestimonialModalComponent, {
      size : "xl"
    });

    modal.componentInstance.testimonialId = id;

  }

  sortList(event:any) {
    this.sort = event.target.value;
    this.listTestimonials();
  }

  publishTestimonial(id:any) {
    this.service.publishTestimonial(id).subscribe((res) => {
      this.listTestimonials();
    })
  }
}
