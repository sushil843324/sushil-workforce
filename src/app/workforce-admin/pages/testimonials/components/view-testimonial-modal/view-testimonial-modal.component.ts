import { Component, Input, OnInit } from '@angular/core';
import { TestimonialsService } from '../../services/testimonials.service';

@Component({
  selector: 'app-view-testimonial-modal',
  templateUrl: './view-testimonial-modal.component.html',
  styleUrls: ['./view-testimonial-modal.component.scss']
})
export class ViewTestimonialModalComponent implements OnInit {

  constructor(
    private service : TestimonialsService
  ) { }

  @Input() testimonialId : any;

  testimonialDetail : any;

  ngOnInit(): void {
    this.TestimonialDetail();
  }

  TestimonialDetail() {
    this.service.getTestimonialDetail(this.testimonialId).subscribe((res) => {
      console.log(this.testimonialDetail, "here");
      this.testimonialDetail = res.data[0];
    })
  }

}
