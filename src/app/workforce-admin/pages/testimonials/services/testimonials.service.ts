import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from 'src/app/organization/interfaces/organization-types';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TestimonialsService {

  constructor(
    private http : HttpClient
  ) { }

  // GET

  getTestimonialsList(isPublished : Boolean, order : string) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/feedbacks/list?isPublished=${isPublished}&sort=${order}`
    )
  }
  
  getTestimonialDetail(testimonialId : any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/feedbacks/get?feedbackId=${testimonialId}`
    )
  }

  publishTestimonial(testimonialId : any) {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/feedbacks/publish?feedbackId=${testimonialId}`
    )
  }

}
