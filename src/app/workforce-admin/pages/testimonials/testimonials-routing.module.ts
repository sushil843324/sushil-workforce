import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestimonialsListComponent } from './components/testimonials-list/testimonials-list.component';
import { PublishedTestimonialsListComponent } from './components/published-testimonials-list/published-testimonials-list.component';

const routes: Routes = [
  {
    path : "list",
    component : TestimonialsListComponent
  },
  {
    path : "published/list",
    component : PublishedTestimonialsListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestimonialsRoutingModule { }
