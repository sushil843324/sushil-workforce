import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestimonialsRoutingModule } from './testimonials-routing.module';
import { TestimonialsListComponent } from './components/testimonials-list/testimonials-list.component';
import { PublishedTestimonialsListComponent } from './components/published-testimonials-list/published-testimonials-list.component';
import { ViewTestimonialModalComponent } from './components/view-testimonial-modal/view-testimonial-modal.component';


@NgModule({
  declarations: [
    TestimonialsListComponent,
    PublishedTestimonialsListComponent,
    ViewTestimonialModalComponent
  ],
  imports: [
    CommonModule,
    TestimonialsRoutingModule
  ]
})
export class TestimonialsModule { }
