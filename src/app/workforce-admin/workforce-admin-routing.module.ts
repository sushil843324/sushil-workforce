import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path : "dashboard",
    loadChildren : () => import("./pages/dashboard/dashboard.module").then(
      (m) => m.DashboardModule
    )
  },
  {
    path : "project-requests",
    loadChildren : () => import("./pages/project-requests/project-requests.module").then(
      (m) => m.ProjectRequestsModule
    )
  },
  {
    path : "published-projects",
    loadChildren : () => import("./pages/published-projects/published-projects.module").then(
      (m) => m.PublishedProjectsModule
    )
  },
  {
    path : "proposals",
    loadChildren : () => import("./pages/proposals/proposals.module").then(
      (m) => m.ProposalsModule
    )
  },
  {
    path : "testimonials",
    loadChildren : () => import("./pages/testimonials/testimonials.module").then(
      (m) => m.TestimonialsModule
    )
  },
  {
    path : "landing-page",
    loadChildren : () => import("./pages/landing-page/landing-page.module").then(
      (m) => m.LandingPageModule
    )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkforceAdminRoutingModule { }
