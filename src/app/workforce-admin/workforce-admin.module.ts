import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkforceAdminRoutingModule } from './workforce-admin-routing.module';


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    WorkforceAdminRoutingModule
  ]
})
export class WorkforceAdminModule { }
